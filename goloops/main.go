// Print i as long as i is less than 6.

package main

import "fmt"

func main() {
	for i := 0; i < 6; i++ {
		fmt.Print(i)
	}
}

// Use a for loop to print "Yes" 5 times:

package main 

import "fmt"

func main () {
	for i := 0; i <5; i++ {
		fmt.Print("Yes")
	}
}

// Stop the loop if for is 5. 

package main 

import "fmt"

func main () {
	for i :=0;i <10 ; i++ {
		if i ==5 {
			break 
		}
	}
}

// In the following loop, when the value is "4", jump directly to the next value using (continue). 

package main 

import "fmt"

func main () {
	for i := 0 ; i <10 ; i ++ {
		if i ==4 {
			continue 
		}
	}
}