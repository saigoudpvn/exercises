// Multiply 10 with 5, and print the result.

package main

import "fmt"

func main() {
	fmt.Println(10 * 5)
}


// Use the correct arithmetic operator to increase the value of the variable x by 1.

package main 

import "fmt"

func main() {
	x := 1
	x++
	fmt.Println(x)
}

// Use assignment operators to add 5 to x. 
package main 

import "fmt"

func main () {
	x:= 1
	x+=5 
	fmt.Print(x)
}