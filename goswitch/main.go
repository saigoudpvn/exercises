// switch statement

package main

import "fmt"

func main() {
	var day = 2
	switch day {
	case (1):
		fmt.Print("Saturday")
	case (2):
		fmt.Print("Sunday")
	}
}

//  if there is no case match in the switch statement

package main

import "fmt"

func main() {
	var day = 2
	switch day {
	case (1):
		fmt.Print("Saturday")
	case (2):
		fmt.Print("Sunday")
	}
	default :
	fmt.Print("Weekday")
}