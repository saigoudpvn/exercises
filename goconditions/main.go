// Print "Hello World" if x is greater than y.

package main

import "fmt"

func main() {
	var x = 50
	var y = 10
	if x > y {
		fmt.Println("Hello world")
	}
}


// Print "Hello World" if x is equal to y. 

package main 


import "fmt"

func main() {
	var x = 50 
	var y = 50 
	if x ==y {
		fmt.Println("Hello World!")
	}
}

// Print "Yes" if x is equal to y, otherwise print "No". 

package main 


import "fmt"

func main() {
	var x = 50 
	var y = 50 
	if x ==y {
		fmt.Print("Yes")
	}else {
		fmt.Print("No")
	}
}

// Print "1" if x is equal to y, print "2" if x is greater than y, otherwise print "3". 

package main 


import "fmt"

func main() {
	var x = 50 
	var y = 50 
	if x ==y {
		fmt.Print("1")
	}else if {
		fmt.Print("2")
	}else {
		fmt.Print("3")
	}
}