// Example - 1 
package main

import "fmt"

func main() {

	x := 0xFF
	y := 0x9C

	fmt.Printf("Type of variable x is %T\n", x)
	fmt.Printf("Value of x in hexadecimal is %X\n", x)
	fmt.Printf("Value of x in decimal is %v\n", x)

	fmt.Printf("Type of variable y is %T\n", y)
	fmt.Printf("Value of y in hexadecimal is %X\n", y)
	fmt.Printf("Value of y in decimal is %v\n", y)

}

// Example -2 (Dereferencing the Pointer)

package main

import "fmt"

func main() {

	// using var keyword
	// we are not defining
	// any type with variable
	var y = 458
	
	// taking a pointer variable using
	// var keyword without specifying
	// the type
	var p = &y

	fmt.Println("Value stored in y = ", y)
	fmt.Println("Address of y = ", &y)
	fmt.Println("Value stored in pointer variable p = ", p)

	// this is dereferencing a pointer
	// using * operator before a pointer
	// variable to access the value stored
	// at the variable at which it is pointing
	fmt.Println("Value stored in y(*p) = ", *p)

}


// Example - 3 (changing the value of the pointer or at the memory location instead of assigning a new value to the variable.)

package main

import "fmt"

func main() {

	// using var keyword
	// we are not defining
	// any type with variable
	var y = 458
	
	// taking a pointer variable using
	// var keyword without specifying
	// the type
	var p = &y

	fmt.Println("Value stored in y before changing = ", y)
	fmt.Println("Address of y = ", &y)
	fmt.Println("Value stored in pointer variable p = ", p)

	// this is dereferencing a pointer
	// using * operator before a pointer
	// variable to access the value stored
	// at the variable at which it is pointing
	fmt.Println("Value stored in y(*p) Before Changing = ", *p)

	// changing the value of y by assigning
	// the new value to the pointer
	*p = 500

	fmt.Println("Value stored in y(*p) after Changing = ",y)

}
