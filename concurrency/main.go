package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var wGroup sync.WaitGroup

func main() {
	wGroup.Add(2)
	go numbers()
	go alphabets()
	// numbers()
	// alphabets()
	wGroup.Wait()

	// time.Sleep(3200 * time.Millisecond)
	fmt.Println("\nmain terminated")

}

func numbers() {
	// func Now() Time
	// returns the current local time; requires "time" package
	// func (t time) UnixNano() int64
	// returns 't' as a Unix time, the number of nanoseconds elapsed.
	// since january 1,1970 UTC; requires "time")
	rand.Seed(time.Now().UnixNano())

	for i := 1; i <= 15; i++ {
		time.Sleep(200 * time.Millisecond)
		fmt.Printf("%d ", rand.Intn(20)+20) // formula :rand.Intn(max-min) + min
	}
	wGroup.Done()
}

func alphabets() {
	for i := 'C'; i <= 'G'; i++ {
		time.Sleep(400 * time.Millisecond)
		fmt.Printf("%c ", i)
	}
	wGroup.Done()
}
