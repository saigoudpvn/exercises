//Comments in Go are written with a special character.

package main

import (
	"fmt"
)

func main() {

	//this is a comment
	fmt.Println("Hello World!")
}


// Multi line comments in Go are written with a special character


package main

import ("fmt")

func main() {
  
/*
 These lines
  are comments  
*/

  fmt.Println("Hello World!")
}