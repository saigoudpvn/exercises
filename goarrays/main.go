// Create an array of type string called cars

package main 

import "fmt"

func main() {
	var cars = [4] string {"volvo","bmw","ford","mazda"}
	fmt.Println(cars)
}

// Print the value of the second element in the cars array. 

package main 

import "fmt"

func main() {
	var cars = [4] string {"volvo","bmw","ford","mazda"}
	fmt.Println(cars[1])
}


// Change the value from "Volvo" to "Opel", in the cars array. 

package main 

import "fmt"

func main() {
	var cars = [4] string {"volvo","bmw","ford","mazda"}
	cars[0] = "Opel"
	fmt.Println(cars)
}