// Create a function named myFunction and call it inside main().

package main

import "fmt"

func myFunction() {
	fmt.Println("I just got promoted!")
}

func main() {
	myFunction()
}


// Insert the missing part to call myFunction two times. 

package main

import "fmt"

func myFunction() {
	fmt.Println("I just got promoted!")
}

func main() {
	myFunction()
	myFunction()
}

// Adding a parameter of type string to myFunction. 

package main
import ("fmt")

func myFunction(fname string) {
  fmt.Printf("%v Doe", fname)
}

func main() {
  myFunction("John")
}
 

// Return the addition of 5 + x. 

package main
import ("fmt")

func myFunction(x int) int {
  return 5 + x
}

func main() {
  fmt.Println(myFunction(3))
}
