// Data types for the following variables.

package main

func main() {
	var myNum int = 90
	var myWord string = "Hello"
	var myBool bool = true
}
