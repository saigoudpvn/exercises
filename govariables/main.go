// Create a variable named myNum and assign the value 50 to it.

package main

import "fmt"

func main() {
	var myNum = 50
	fmt.Println(myNum)

}


// Create a variable named myWord and assign "Hello!" to it.

package main 

import "fmt"

func main () {
	var myWord = "Hello!"
	fmt.Println("myWord")
}


// Create a variable called z, assign x + y to it, and display the result. 

package main
import ("fmt")

func main() {
  var x = 5
  var y = 10
  var z =  x + y
  fmt.Println(z)
}

//  create three variables of the same type. The three variables should have the following values: 5, 10, and 15. 

package main
import ("fmt")

func main() {
  var x, y, z= 5, 10, 15

  fmt.Println(x, y, z)
}

// 